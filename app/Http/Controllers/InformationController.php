<?php

namespace App\Http\Controllers;
use App\Models\appartient;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class InformationController extends Controller
{
    public function index($id){
        $pieces = DB::table('appartients')
                    ->select('*')
                    ->crossJoin('actes')
                   ->crossJoin('pieces')
                    ->where('appartients.id_acte','=',$id)
                    ->where('appartients.id_acte','=',DB::raw('actes.id_acte'))
                    ->where('appartients.id_piece','=',DB::raw('pieces.id'))
                    ->get();

                    $choice = DB::table('actes')
                    ->select('libelle')
                    ->where('id_acte','=',$id)
                    ->value(0);

                    // $pieceChoice = DB::table('pieces')
                    // ->select('libelle_piece')
                    // ->where('id','=','appartients.id_piece')
                    // ->value(0);

        return view('pages.information')
                    ->with('pieces', $pieces)
                    ->with('choice', $choice);
                   // ->with('pieceChoice', $pieceChoice);
    }
}
