<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

    <head>

        <!-- Vendor CSS Files -->
        <link href="{{ asset('assets_menu/vendor/boxicons/css/boxicons.min.css') }}" rel="stylesheet">

        <!-- Template Main CSS File -->
        <link href="{{ asset('assets_menu/css/style.css') }}" rel="stylesheet">

    </head>

    <body>
        <div class="container-fluid">

            <button type="button" class="nav-toggle"><i class="bx bx-menu"></i></button>

            <nav class="nav-menu">
                <ul>
                    <li class="">
                        <a href="{{url('/suivi')}}" class="scrollto">Suivre demande</a>
                    </li>
                    <li>
                        <a href="{{url('/admin')}}" class="scrollto">Administration</a>
                    </li>
                </ul>
            </nav><!-- .nav-menu -->

        </div>

        <!-- Template Main JS File -->
        <script src="{{ asset('assets_menu/js/main.js') }}"></script>
    </body>

</html>
