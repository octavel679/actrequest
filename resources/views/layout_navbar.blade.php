@section('header')
    <header id="header" class="header-top header-top-margin">
        <h1 class="pl-5"><a href="{{url('/')}}" class="mr-auto">ENEAM <img style="height: 1.5em; width:1.5em;" src="{{asset('assets/img/logo.png')}}" alt="" class="img-fluid"></a></h1>
        <nav id="navbar" class="navbar">
            <ul>
                <li><a class="nav-link {{(Str::contains(Request::path(), "acte")) ? 'active' : '' }}">Actes</a></li>
                <li><a class="nav-link {{(Str::contains(Request::path(), "information")) ? 'active' : '' }}">Informations</a></li>
                <li><a class="nav-link {{(Str::contains(Request::path(), "scolaire")) ? 'active' : '' }}">Scolaires</a></li>
                <li><a class="nav-link {{(Str::contains(Request::path(), "fichier")) ? 'active' : '' }}">Fichiers</a></li>
                <li><a class="nav-link {{(Str::contains(Request::path(), "recapitulatif")) ? 'active' : '' }}">Récapitulatif</a></li>
            </ul>
            <i class="bi bi-list mobile-nav-toggle"></i>
        </nav>
        <!-- .navbar -->
    </header>
@endsection
