<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDemandesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('demandes', function (Blueprint $table) {
            $table->id();
            $table->integer('matricule');
            $table->string('nom');
            $table->string('prenom');
            $table->string('email');
            $table->integer('telephone');
            $table->string('sexe');
            $table->string('code');
            $table->date('date');
            $table->integer('id_acte')->unsigned();
            $table->foreign('id_acte')
                  ->references('id')
                  ->on('actes')
                  ->onDelete('restrict')
                  ->onUpdate('restrict');
            $table->integer('id_classe')->unsigned();
            $table->foreign('id_classe')
                  ->references('id')
                  ->on('classes')
                  ->onDelete('restrict')
                  ->onUpdate('restrict');
            $table->integer('id_utilisateur')->unsigned();
            $table->foreign('id_utilisateur')
                        ->references('id')
                        ->on('utilisateurs')
                        ->onDelete('restrict')
                        ->onUpdate('restrict');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('demandes');
    }
}
