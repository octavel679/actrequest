<?php //Fonction d'écoute de l'url
    $url = $_SERVER['REQUEST_URI'];
?>

@extends('layout')
@extends('layout_navbar')

@section('head')
    <head>
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1.0" name="viewport">

        <title>ENEAM - Demande d'actes en ligne</title>
        <meta content="" name="description">
        <meta content="" name="keywords">

        <!-- Favicons -->
        <link href="{{asset('assets/img/favicon.png')}}" rel="icon">
        <link href="{{asset('assets/img/apple-touch-icon.png')}}" rel="apple-touch-icon">

        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

        <!-- Vendor CSS Files -->
        <link href="{{asset('assets/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{asset('assets/vendor/bootstrap-icons/bootstrap-icons.css')}}" rel="stylesheet">
        <link href="{{asset('assets/vendor/boxicons/css/boxicons.min.css')}}" rel="stylesheet">
        <link href="{{asset('assets/vendor/glightbox/css/glightbox.min.css')}}" rel="stylesheet">
        <link href="{{asset('assets/vendor/remixicon/remixicon.css')}}" rel="stylesheet">
        <link href="{{asset('assets/vendor/swiper/swiper-bundle.min.css')}}" rel="stylesheet">

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

        <!-- Template Main CSS File -->
        <link href="{{asset('stylepersonal.css')}}" rel="stylesheet">
    </head>
@endsection

@section('portfolio section')
<div class="services special">

         <div class="container-special">
            <div class="section-title">
                <h2>&agrave; T&eacute;l&eacute;charger</h2>
                <p>Les pi&egrave;ces</p>
            </div>

            <form action="" method="get" class="php-email-form php-email2-form mt-4">
                @csrf
                <div class="row">
                   @foreach ($pieces as $piece)
                        <div class="col-lg-4 col-md-6 d-flex align-items-stretch style=" style="margin-bottom: 2%">
                            <div class="icon-box image-upload">
                                <label for="file-input">
                                    <div class="icon hovermouse"><i class="bx bx-file"></i></div>
                                    <h4><a href="">{{$piece->libelle_piece }}</a></h4>
                                    <p>Voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi</p>
                                </label>
                                <input type="file" id="file-input">
                            </div>
                        </div>
                  @endforeach
                    {{-- <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4 mt-md-0">
                        <div class="icon-box image-upload">
                            <label for="file-input2">
                                <div class="icon hovermouse"><i class="bx bx-file"></i></div>
                                <h4><a href="">Quittance de banque</a></h4>
                                <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore</p>
                            </label>
                            <input type="file" id="file-input2">
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4 mt-lg-0">
                        <div class="icon-box image-upload">
                            <label for="file-input3">
                                <div class="icon hovermouse"><i class="bx bx-tachometer"></i></div>
                                <h4><a href="">Acte de naissance</a></h4>
                                <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia</p>
                            </label>
                            <input type="file" id="file-input3">
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4">
                        <div class="icon-box image-upload">
                            <label for="file-input4">
                                <div class="icon hovermouse"><i class="bx bx-world"></i></div>
                                <h4><a href="">Demande manuscrite</a></h4>
                                <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis</p>
                            </label>
                            <input type="file" id="file-input4">
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4">
                        <div class="icon-box image-upload">
                            <label for="file-input5">
                                <div class="icon hovermouse"><i class="bx bx-slideshow"></i></div>
                                <h4><a href="">Carte d'identit&eacute;</a></h4>
                                <p>Quis consequatur saepe eligendi voluptatem consequatur dolor consequuntur</p>
                            </label>
                            <input type="file" id="file-input5">
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6 d-flex align-items-stretch mt-4">
                        <div class="icon-box image-upload">
                            <label for="file-input6">
                                <div class="icon hovermouse"><i class="bx bx-arch"></i></div>
                                <h4><a href="">Carte d'&eacute;tudiant</a></h4>
                                <p>Modi nostrum vel laborum. Porro fugit error sit minus sapiente sit aspernatur</p>
                            </label>
                            <input type="file" id="file-input6">
                        </div>
                    </div> --}}
                </div>
                <div class="contact section-show">
                    <div class="php-email2-form">
                        <div class="form-group">
                            <div class="text-left">
                                <div class="row">
                                    <div class="col-lg-12 text-center">
                                        <a href="{{url('/recapitulatif/' .$piece->id_acte. '/')}}" class="nav-link">
                                            <button>Envoyer</button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- <div class="text-right mt-3">
                    <a href="#recapitulatif" class="nav-link">
                    <button>Suivant</button></a>
                </div> --}}
            </form>
        </div>

@endsection
@section('designed')
    <div class="credits">
        Designed by <a href="#">Delyre, LHAM & Octavel</a>
    </div>
@endsection

