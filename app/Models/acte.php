<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


/**
 * @property integer $id
 * @property string $libelle
 * 
 */

class acte extends Model
{
    use HasFactory;
    /**
     * @var array
     */
    protected $fillable = ['libelle'];

}
