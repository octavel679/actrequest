
@extends('layout')
@extends('layout_actes')
@extends('layout_navbar')

@section('head')
    <head>
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1.0" name="viewport">

        <title>ENEAM - Demande d'actes en ligne</title>
        <meta content="" name="description">
        <meta content="" name="keywords">

        <!-- Favicons -->
        <link href="{{asset('assets/img/favicon.png')}}" rel="icon">
        <link href="{{asset('assets/img/apple-touch-icon.png')}}" rel="apple-touch-icon">

        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

        <!-- Vendor CSS Files -->
        <link href="{{asset('assets/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{asset('assets/vendor/bootstrap-icons/bootstrap-icons.css')}}" rel="stylesheet">
        <link href="{{asset('assets/vendor/boxicons/css/boxicons.min.css')}}" rel="stylesheet">
        <link href="{{asset('assets/vendor/glightbox/css/glightbox.min.css')}}" rel="stylesheet">
        <link href="{{asset('assets/vendor/remixicon/remixicon.css')}}" rel="stylesheet">
        <link href="{{asset('assets/vendor/swiper/swiper-bundle.min.css')}}" rel="stylesheet">

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

        <!-- Template Main CSS File -->
        <link href="{{asset('stylepersonal.css')}}" rel="stylesheet">
    </head>
@endsection

@section('resume section')
    <div class="resume special">
        <div class="container-special">
            <div class="special-title">
                <h2>Quelles pièces dois-je fournir ?</h2>
                    <p>{{$choice}}</p>
            </div>
            <div class="row">

                <div class="col-lg-6">
                    {{-- <h3 class="resume-title">{{$piece->commentaire}}</h3> --}}
               @foreach ($pieces as $piece)
                  <div class="resume-item pb-0">
                        <h4>{{$piece->libelle_piece }}</h4>
                        <p><em>Innovative and deadline-driven Graphic Designer with 3+ years of experience designing and developing user-centered digital/print marketing material from initial concept to final, polished deliverable.</em></p>

                    </div>
               @endforeach</div>

            <div class="col-lg-6">
                <h3 class="resume-title">Quel format utiliser pour ces pièces ?</h3>
                <div class="resume-item">
                    <p>
                        Toutes les pièces à télécharger se doivent d'être dans l'un des formats standards de
                        lecture que sont : PDF, JPG, PNG, SVG, BMP, etc.
                        <br> <span class="text-white">Veillez à la <strong>clarté</strong> et à la <strong>lisibilité</strong> des fichiers !</span>
                    </p>
                </div>

                </div>
        </div>


        <div class="contact section-show">
            <div class="php-email2-form ">
                <div class="form-group">
                    <div class="text-left">
                        <div class="row">
                            <div class="col-lg-12 text-center">
                                <a href="{{url('/scolaire/'.$piece->id_acte.'/')}}" class="nav-link">
                                    <button type="submit">
                                        Suivant
                                    </button>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('designed')
    <div class="credits">
        Designed by <a href="#">Delyre, LHAM & Octavel</a>
    </div>
@endsection

