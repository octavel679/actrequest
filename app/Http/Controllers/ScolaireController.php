<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\Http\Requests;
use App\Models\classe;
use App\Models\demande;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Http\Controllers\Controller;

class ScolaireController extends Controller
{
    //
    public function index($choix){
        $classes = classe::all();
        return view('pages.scolaire')
                ->with('choix', $choix)
                ->with('classes', $classes);
    }

    public function store($id, Request $request){
        //DB::insert('insert into dem demande () values ()', []);
        $matricule = $request->input('matricule');
        $nom = $request->input('nom');
        $prenom = $request->input('prenom');
        $num = $request->input('telephone');
        $mail = $request->input('email_etudiant');
        $sexe = $request->input('sexe');
        $now = Carbon::today();
        $ida = $id;
        $idc = $request->input('classe');

        $data = array(
            "matricule" => $matricule,
            "nom" => $nom,
            "prenom" => $prenom,
            "telephone" => $num,
            "email_etudiant" => $mail,
            "sexe" => $sexe,
            "date" => $now,
            "id_acte" => $ida,
            "id_classe" => $idc
        );
        DB::table('demandes')->insert($data);


        return redirect('/fichier/'.$id.'/');
    }

}
