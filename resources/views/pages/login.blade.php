<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>ENEAM - Connexion</title>
    <!-- inject:css -->
    <link rel="stylesheet" href="{{asset('assets_dashboard/css/vertical-layout-light/style.css')}}">
    <!-- endinject -->
    <link rel="shortcut icon" href="{{asset('assets/img/favicon.png')}}"/>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="{{asset('assets_dashboard/js/dashboard.js')}}"></script>
</head>

<body>
  <div class="container-scroller">
    <div class="container-fluid page-body-wrapper full-page-wrapper">
      <div class="content-wrapper d-flex align-items-center auth px-0 bg-login">
        <div class="row w-100 mx-0">
          <div class="col-lg-4 mx-auto">
            <div class="auth-form-light text-center py-5 px-4 px-sm-5">
              <div class="brand-logo">
                  <a href="{{url('/')}}">
                    <img src="{{asset('assets/img/apple-touch-icon.png')}}" alt="logo">
                </a>
              </div>
              <h4>Connectez-vous pour continuer !</h4>
              <form class="pt-5" action="login" method="POST">
                    @csrf
                    <div class="form-group">
                    <input type="email" class="form-control form-control-lg" id="exampleInputEmail1" name="email" placeholder="Email">
                    </div>
                    <div class="form-group">
                    <input type="password" class="form-control form-control-lg" id="exampleInputPassword1" name="password" placeholder="Mot de passe">
                    </div>
                    <div class="mt-5 align-center">
                    <button class="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn" type="submit">Se Connecter</button>
                    </div>
                    <div class="mt-4 justify-content-between align-items-center">
                        <a href="{{url('/reset-password')}}" class="auth-link text-black">Mot de passe oublié ?</a>
                    </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <!-- content-wrapper ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
  <!-- plugins:js -->
  <!-- endinject -->
</body>

</html>
