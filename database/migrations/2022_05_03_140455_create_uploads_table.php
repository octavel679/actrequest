<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUploadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('uploads', function (Blueprint $table) {
            $table->id();
            $table->string('fichier');
            $table->integer('id_demande')->unsigned();
            $table->foreign('id_demande')
                  ->references('id')
                  ->on('demandes')
                  ->onDelete('restrict')
                  ->onUpdate('restrict');
            $table->integer('id_piece')->unsigned();
            $table->foreign('id_piece')
                    ->references('id')
                    ->on('pieces')
                    ->onDelete('restrict')
                    ->onUpdate('restrict');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('uploads');
    }
}
