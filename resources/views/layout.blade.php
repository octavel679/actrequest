<!DOCTYPE html>
<html lang="en">

@yield('head')

<body>

  <!-- ======= Header ======= -->
@yield('header')

@yield('bulletin section')
 <!-- End Resume Section -->

@yield('diplome section')
 <!-- End Resume Section -->

@yield('certificat1 section')
 <!-- End Resume Section -->

@yield('certificat2 section')
 <!-- End Resume Section -->

  <!-- ======= Resume Section ======= -->
@yield('resume section')
  <!-- End Resume Section -->

  <!-- ======= Services Section ======= -->
@yield('scolaire section')
  <!-- End Services Section -->

  <!-- ======= Portfolio Section ======= -->
@yield('portfolio section')
  <!-- End Portfolio Section -->

  <!-- ======= Contact Section ======= -->
@yield('recapitulatif section')
  <!-- End Contact Section -->

@yield('designed')

  <!-- Vendor JS Files -->

</body>

</html>
