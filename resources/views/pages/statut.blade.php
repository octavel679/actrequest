<!DOCTYPE html>
<html lang="en">
<head>
	<title>ENEAM - Demande d'acte</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
	<link rel="icon" type="image/ico" href="{{asset('assets/img/favicon.ico')}}"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('assets_suivi/vendor/bootstrap/css/bootstrap.min.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('assets_suivi/fonts/font-awesome-4.7.0/css/font-awesome.min.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('assets_suivi/vendor/animate/animate.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('assets_suivi/vendor/select2/select2.min.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('assets_suivi/css/util.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('assets_suivi/css/main.css')}}">
<!--===============================================================================================-->
</head>
<body>

	<!--  -->
	<div class="simpleslide100">
		<div class="simpleslide100-item bg-img1" style="background-image: url({{asset('assets/img/bg.jpg')}});"></div>
        <div class="simpleslide100-item bg-img1" style="background-image: url({{asset('assets/img/bg.jpg')}});"></div>
        <div class="simpleslide100-item bg-img1" style="background-image: url({{asset('assets/img/bg.jpg')}});"></div>
	</div>
    {{-- public\assets_suivi\images\bg01.jpg
    D:\Swipe\ENEAM\3ème année\UML Avancé\Projet\actrequest\public\assets_suivi\images\bg01.jpg --}}

	<div class="size1 overlay1">
		<!--  -->
		<div class="size1 flex-col-c-m p-l-15 p-r-15 p-t-50 p-b-50">
            <h3 class="l1-txt1 txt-center p-b-25">
                <img src="{{asset('assets/img/logo.png')}}" style="width: 60px; height:60px;">
				ENEAM
			</h3>

			<p class="m2-txt1 txt-center p-b-48">
				Statut actuel de votre demande :
			</p>

			<div class="flex-w flex-c-m cd100">
				<div class="flex-col-c-m size2 bor1 m-l-15 m-r-15 m-b-10">
					<span class="l2-txt1 p-b-9 days"><img src="{{asset('assets_suivi/images/'.$statutimage.'.png')}}" style="width: 160px; height:160px;"></span>
				</div>
			</div>

            <div class="mt-1 text-white text-center p-b-33">
				<h3>{{$statuttexte}} !</h3>
			</div>


			<form class="w-full flex-w flex-c-m validate-form">
                <a href="{{url('/suivi')}}">
                    <button class="flex-c-m size3 s2-txt3 how-btn1 trans-04 where1">
                        Retour
                    </button>
                </a>
			</form>

		</div>
	</div>





<!--===============================================================================================-->
	<script src="{{asset('assets_suivi/vendor/jquery/jquery-3.2.1.min.js')}}"></script>
<!--===============================================================================================-->
	<script src="{{asset('assets_suivi/vendor/bootstrap/js/popper.js')}}"></script>
	<script src="{{asset('assets_suivi/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
<!--===============================================================================================-->
	<script src="{{asset('assets_suivi/vendor/select2/select2.min.js')}}"></script>
<!--===============================================================================================-->
	<script src="{{asset('assets_suivi/vendor/countdowntime/moment.min.js')}}"></script>
	<script src="{{asset('assets_suivi/vendor/countdowntime/moment-timezone.min.js')}}"></script>
	<script src="{{asset('assets_suivi/vendor/countdowntime/moment-timezone-with-data.min.js')}}"></script>
	<script src="{{asset('assets_suivi/vendor/countdowntime/countdowntime.js')}}"></script>
<!--===============================================================================================-->
	<script src="{{asset('assets_suivi/vendor/tilt/tilt.jquery.min.js')}}"></script>
	<script >
		$('.js-tilt').tilt({
			scale: 1.1
		})
	</script>
<!--===============================================================================================-->
	<script src="{{asset('assets_suivi/js/main.js')}}"></script>

</body>
</html>
