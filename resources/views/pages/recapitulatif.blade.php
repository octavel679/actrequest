@extends('layout')
@extends('layout_navbar')

@section('head')
    <head>
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1.0" name="viewport">

        <title>ENEAM - Demande d'actes en ligne</title>
        <meta content="" name="description">
        <meta content="" name="keywords">

        <!-- Favicons -->
        <link href="{{asset('assets/img/favicon.png')}}" rel="icon">
        <link href="{{asset('assets/img/apple-touch-icon.png')}}" rel="apple-touch-icon">

        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

        <!-- Vendor CSS Files -->
        <link href="{{asset('assets/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{asset('assets/vendor/bootstrap-icons/bootstrap-icons.css')}}" rel="stylesheet">
        <link href="{{asset('assets/vendor/boxicons/css/boxicons.min.css')}}" rel="stylesheet">
        <link href="{{asset('assets/vendor/glightbox/css/glightbox.min.css')}}" rel="stylesheet">
        <link href="{{asset('assets/vendor/remixicon/remixicon.css')}}" rel="stylesheet">
        <link href="{{asset('assets/vendor/swiper/swiper-bundle.min.css')}}" rel="stylesheet">

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

        <!-- Template Main CSS File -->
        <link href="{{asset('stylepersonal.css')}}" rel="stylesheet">
    </head>
@endsection

@section('recapitulatif section')
    <div class="resume special">
        <div class="container-special">
            <div class="row">
                <h1>RECAPITULATIF</h1>

                <div class="col-lg-6">
                    @foreach ($demandes as $demande)
                        <div class="resume-item pb-0">
                            <h4>Acte choisis</h4>
                            <p><em>{{ $demande->libelle }}</em></p>
                        </div>
                        <div class="resume-item pb-0">
                            <h4>Matricule</h4>
                            <p><em>{{ $demande->matricule }}</em></p>
                        </div>
                        <div class="resume-item pb-0">
                            <h4>Mail</h4>
                            <p><em>{{ $demande->email_etudiant }}</em></p>
                        </div>
                        <div class="resume-item pb-0">
                            <h4>Nom</h4>
                            <p><em>{{ $demande->nom }}</em></p>
                        </div>
                        <div class="resume-item pb-0">
                            <h4>Prenom</h4>
                            <p><em>{{ $demande->prenom }}</em></p>
                        </div>
                        <div class="resume-item pb-0">
                            <h4>Numero de telephone</h4>
                            <p><em>{{ $demande->telephone }}</em></p>
                        </div>
                        <div class="resume-item pb-0">
                            <h4>Classe</h4>
                            <p><em>{{ $demande->libelle_classes }}</em></p>
                        </div>
                        <div class="resume-item pb-0">
                            <h4>Sexe</h4>
                            <p><em>{{ $demande->sexe }}</em></p>
                        </div>
                    @endforeach
                </div>

                <div class="col-lg-6">
                    <h3 class="resume-title">Enregistrer les informations</h3>
                    <div class="resume-item">
                        <p>
                            <br> <span class="text-white">N'oubliez pas de<strong> sauvegarder</strong> le <strong>code</strong> à la fin.</span>
                        </p>
                    </div>
                </div>

                <div class="contact section-show">
                    <div class="php-email2-form ">
                        <div class="form-group">
                            <div class="text-left">
                                <div class="row">
                                    <div class="col-lg-12 text-center">
                                        <a href="{{url('recapsave/'.$id.'/'.$matricule.'/')}}" class="nav-link">
                                            <button type="submit">
                                                Enregistrer
                                            </button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('designed')
    <div class="credits">
        Designed by <a href="#">Delyre, LHAM & Octavel</a>
    </div>
@endsection
