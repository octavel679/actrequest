<?php //Fonction d'écoute de l'url
    $url = $_SERVER['REQUEST_URI'];
?>

@extends('layout')
@extends('layout_actes')
@extends('layout_navbar')

@section('head')
    <head>
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1.0" name="viewport">

        <title>ENEAM - Demande d'actes en ligne</title>
        <meta content="" name="description">
        <meta content="" name="keywords">

        <!-- Favicons -->
        <link href="{{asset('assets/img/favicon.png')}}" rel="icon">
        <link href="{{asset('assets/img/apple-touch-icon.png')}}" rel="apple-touch-icon">

        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

        <!-- Vendor CSS Files -->
        <link href="{{asset('assets/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{asset('assets/vendor/bootstrap-icons/bootstrap-icons.css')}}" rel="stylesheet">
        <link href="{{asset('assets/vendor/boxicons/css/boxicons.min.css')}}" rel="stylesheet">
        <link href="{{asset('assets/vendor/glightbox/css/glightbox.min.css')}}" rel="stylesheet">
        <link href="{{asset('assets/vendor/remixicon/remixicon.css')}}" rel="stylesheet">
        <link href="{{asset('assets/vendor/swiper/swiper-bundle.min.css')}}" rel="stylesheet">

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

        <!-- Template Main CSS File -->
        <link href="{{asset('stylepersonal.css')}}" rel="stylesheet">
    </head>
@endsection

@section('about section')
    <div class="resume special">
         <div class="container-special">

            <div class="row">
                <div class="container text-center" id="navbar">
                    <h2>
                        Demandez votre acte en un seul clic
                    </h2>
                           <p>
                    Choisissez l'acte dont vous avez besoin et suivez les instructions pour demander votre acte.
                          </p>
                    <div class="row stats-row">
                        @foreach($actes as $acte)
                            <div class="stats-col text-center col-md-3 col-sm-6" >
                                <a href="{{url('/information/'.$acte->id_acte.'/')}}" class="nav-link">
                                    <div class="circle">
                                        {{-- <button class="btn-circle"> --}}
                                        <span  class="stats-no" >
                                            <i class="bi bi-plus-lg"></i>
                                        </span>
                                        {{$acte->libelle}}
                                        </div>
                                    {{-- </button> --}}
                                   </a>
                            </div>
                       @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('designed')
    <div class="credits">
        Designed by <a href="#">Delyre, LHAM & Octavel</a>
    </div>
@endsection

