@extends('layout_dashboard')

@section('head')
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>ENEAM - Dashboard</title>
        <!-- plugins:css -->
        <link rel="stylesheet" href="{{asset('assets_dashboard/vendors/feather/feather.css')}}">
        <link rel="stylesheet" href="{{asset('assets_dashboard/vendors/mdi/css/materialdesignicons.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets_dashboard/vendors/ti-icons/css/themify-icons.csss')}}">
        <link rel="stylesheet" href="{{asset('assets_dashboard/vendors/typicons/typicons.css')}}">
        <link rel="stylesheet" href="{{asset('assets_dashboard/vendors/simple-line-icons/css/simple-line-icons.css')}}">
        <link rel="stylesheet" href="{{asset('assets_dashboard/vendors/css/vendor.bundle.base.css')}}">
        <!-- endinject -->
        <!-- Plugin css for this page -->
        <link rel="stylesheet" href="{{asset('assets_dashboard/vendors/datatables.net-bs4/dataTables.bootstrap4.css')}}">
        <link rel="stylesheet" href="{{asset('assets_dashboard/js/select.dataTables.min.css')}}">
        <!-- End plugin css for this page -->
        <!-- inject:css -->
        <link rel="stylesheet" href="{{asset('assets_dashboard/css/vertical-layout-light/style.css')}}">
        <!-- endinject -->
        <link rel="shortcut icon" href="{{asset('assets/img/favicon.png')}}"/>
        <!-- Sweet Alert -->
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    </head>
@endsection

@section('navbar')
    <nav class="navbar default-layout col-lg-12 col-12 p-0 fixed-top d-flex align-items-top flex-row">
        <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-start">
        <div class="me-3">
            <button class="navbar-toggler navbar-toggler align-self-center" type="button" data-bs-toggle="minimize">
            <span class="icon-menu"></span>
            </button>
        </div>
        <div>
            <a class="navbar-brand brand-logo" href="{{url('/admin')}}">
                <h2 style="font-weight:800; margin-bottom:0.1px;">ENEAM</h2 style="font-weight:bold; margin-bottom:0;">
            </a>
            <a class="navbar-brand brand-logo-mini" href="{{url('/admin')}}">
            <img src="{{asset('assets/img/favicon.png')}}" alt="logo"/>
            </a>
        </div>
        </div>
        <div class="navbar-menu-wrapper d-flex align-items-top">
        <ul class="navbar-nav">
            <li class="nav-item font-weight-semibold d-none d-lg-block ms-0">
            <h1 class="welcome-text">Bonjour, <span class="text-black fw-bold">{{session('user')}}</span></h1>
            <h3 class="welcome-sub-text">Votre tableau de bord </h3>
            </li>
        </ul>
        <ul class="navbar-nav ms-auto">
            <li class="nav-item">
            <form class="search-form" action="#">
                <i class="icon-search"></i>
                <input type="search" class="form-control" placeholder="Search Here" title="Search here">
            </form>
            </li>
            <li class="nav-item dropdown">
            <a class="nav-link count-indicator" id="countDropdown" href="#" data-bs-toggle="dropdown" aria-expanded="false">
                <i class="icon-bell"></i>
                <span class="count"></span>
            </a>
            <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list pb-0" aria-labelledby="countDropdown">
                <a class="dropdown-item py-3" href="{{url('/admin#idle')}}">
                <p class="mb-0 font-weight-medium float-left">+ 5 nouvelles commandes</p>
                <span class="badge badge-pill badge-primary float-right">Tout voir</span>
                </a>
                <div class="dropdown-divider"></div>

            </div>
            </li>
            <li class="nav-item dropdown d-none d-lg-block user-dropdown">
            <a class="nav-link" id="UserDropdown" href="#" data-bs-toggle="dropdown" aria-expanded="false">
                <img class="img-xs rounded-circle" src="{{asset('assets_dashboard/images/faces/me.png')}}" alt="Profile image"> </a>
            <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="UserDropdown">
                <div class="dropdown-header text-center">
                <img class="img-md rounded-circle" style="width:32px; height:32px;" src="{{asset('assets_dashboard/images/faces/me.png')}}" alt="Profile image">
                <p class="mb-1 mt-3 font-weight-semibold">{{session('user')}}</p>
                </div>
                <a class="dropdown-item"><i class="dropdown-item-icon mdi mdi-account-outline text-primary me-2"></i> Mes informations</a>
                <a class="dropdown-item"><i class="dropdown-item-icon mdi mdi-message-text-outline text-primary me-2"></i> Mes Messages</a>
                <a class="dropdown-item" onclick="JeMeDeconnecte()"><i class="dropdown-item-icon mdi mdi-power text-primary me-2"></i>Se déconnecter</a>
            </div>
            </li>
        </ul>
        <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-bs-toggle="offcanvas">
            <span class="mdi mdi-menu"></span>
        </button>
        </div>
    </nav>
@endsection

@section('siderbar')
    <!-- partial -->
    <!-- partial:partials/_sidebar.html -->
    <nav class="sidebar sidebar-offcanvas" id="sidebar">
        <ul class="nav">

        <li class="nav-item">
            <a class="nav-link {{(Request::path()=="admin") ? 'active' : '' }}" href="{{url('/admin')}}">
            <i class="mdi mdi-grid-large menu-icon"></i>
            <span class="menu-title">Dashboard</span>
            </a>
        </li>

        <li class="nav-item nav-category">Catégories</li>
        <li class="nav-item">
            <a class="nav-link" data-bs-toggle="collapse" href="#" aria-expanded="false" aria-controls="form-elements">
            <i class="menu-icon mdi mdi-book"></i>
            <span class="menu-title">Diplôme</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-bs-toggle="collapse" href="#" aria-expanded="false" aria-controls="charts">
            <i class="menu-icon mdi mdi-card-text"></i>
            <span class="menu-title">Attestation</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-bs-toggle="collapse" href="#tables" aria-expanded="false" aria-controls="tables">
            <i class="menu-icon mdi mdi-receipt"></i>
            <span class="menu-title">Fiche</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-bs-toggle="collapse" href="#icons" aria-expanded="false" aria-controls="icons">
            <i class="menu-icon mdi mdi-library-books"></i>
            <span class="menu-title">Bulletin</span>
            </a>
        </li>
        <li class="nav-item nav-category">utilisateur</li>
        <li class="nav-item">
            <a class="nav-link" data-bs-toggle="collapse" href="#">
            <i class="menu-icon mdi mdi-account-circle-outline"></i>
            <span class="menu-title">Informations</span>
            </a>
        </li>
        <li class="nav-item nav-category">Aide</li>
        <li class="nav-item">
            <a class="nav-link" data-bs-toggle="collapse" href="#">
                <i class="menu-icon mdi mdi-comment-alert"></i>
                <span class="menu-title">Plainte</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-bs-toggle="collapse" href="#" onclick="JeMeDeconnecte()">
                <i class="menu-icon mdi mdi-power"></i>
                <span class="menu-title">Se déconnecter</span>
            </a>
        </li>
        </ul>
    </nav>
@endsection

@section('idle')
    <div class="d-sm-flex align-items-center justify-content-between border-bottom">
        <ul class="nav nav-tabs" role="tablist">
            <li class="nav-item">
                <a class="nav-link active ps-0" id="home-tab" data-bs-toggle="tab" href="#idle" role="tab" aria-controls="overview" aria-selected="true">En attente</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="profile-tab" data-bs-toggle="tab" href="#current" role="tab" aria-selected="false">En cours</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="contact-tab" data-bs-toggle="tab" href="#done" role="tab" aria-selected="false">Traitées</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="contact-tab" data-bs-toggle="tab" href="#stats" role="tab" aria-controls="overview" aria-selected="false">Statistiques</a>
            </li>
        </ul>
        <div>
        <div class="btn-wrapper">
            <a href="#" class="btn btn-otline-dark align-items-center"><i class="icon-share"></i> Partager</a>
            <a href="#" class="btn btn-primary text-white me-0"><i class="icon-download"></i> Exporter</a>
        </div>
        </div>
    </div>
@endsection

@section('first_row')
    <div class="row">
        <div class="col-lg-8 d-flex flex-column">
        <div class="row flex-grow">
            <div class="col-12 col-lg-4 col-lg-12 grid-margin stretch-card">
            <div class="card card-rounded">
                <div class="card-body">
                <div class="d-sm-flex justify-content-between align-items-start">
                    <div>
                    <h4 class="card-title card-title-dash">Point global</h4>
                    <h5 class="card-subtitle card-subtitle-dash">Tous vos traitements effectués</h5>
                    </div>
                    <div id="performance-line-legend"></div>
                </div>
                <div class="chartjs-wrapper mt-5">
                    <canvas id="performaneLine"></canvas>
                </div>
                </div>
            </div>
            </div>
        </div>
        </div>
        <div class="col-lg-4 d-flex flex-column">
        <div class="row flex-grow">
            <div class="col-md-6 col-lg-12 grid-margin stretch-card">
            <div class="card bg-primary card-rounded">
                <div class="card-body pb-0">
                <h4 class="card-title card-title-dash text-white mb-4">Cette semaine</h4>
                <div class="row">
                    <div class="col-sm-4">
                    <p class="status-summary-ight-white mb-1">Demandes traitées</p>
                    <h2 class="text-info">22</h2>
                    </div>
                    <div class="col-sm-8">
                    <div class="status-summary-chart-wrapper pb-4">
                        <canvas id="status-summary"></canvas>
                    </div>
                    </div>
                </div>
                </div>
            </div>
            </div>
            <div class="col-md-6 col-lg-12 grid-margin stretch-card">
            <div class="card card-rounded">
                <div class="card-body">
                <div class="row">
                    <div class="col-sm-6">
                    <div class="d-flex justify-content-between align-items-center mb-2 mb-sm-0">
                        <div class="circle-progress-width">
                        <div id="totalVisitors" class="progressbar-js-circle pr-2"></div>
                        </div>
                        <div>
                        <p class="text-small mb-2">Demandes</p>
                        <h4 class="mb-0 fw-bold">850</h4>
                        </div>
                    </div>
                    </div>
                    <div class="col-sm-6">
                    <div class="d-flex justify-content-between align-items-center">
                        <div class="circle-progress-width">
                        <div id="visitperday" class="progressbar-js-circle pr-2"></div>
                        </div>
                        <div>
                        <p class="text-small mb-2">En cours</p>
                        <h4 class="mb-0 fw-bold">29</h4>
                        </div>
                    </div>
                    </div>
                </div>
                </div>
            </div>
            </div>
        </div>
        </div>
    </div>
@endsection

{{-- @section('to_do')
    <div class="row flex-grow">
        <div class="col-12 grid-margin stretch-card">
        <div class="card card-rounded">
            <div class="card-body">
            <div class="row">
                <div class="col-lg-12">
                <div class="d-flex justify-content-between align-items-center">
                    <h4 class="card-title card-title-dash">Todo list</h4>
                    <div class="add-items d-flex mb-0">
                    <!-- <input type="text" class="form-control todo-list-input" placeholder="What do you need to do today?"> -->
                    <button class="add btn btn-icons btn-rounded btn-primary todo-list-add-btn text-white me-0 pl-12p"><i class="mdi mdi-plus"></i></button>
                    </div>
                </div>
                <div class="list-wrapper">
                    <ul class="todo-list todo-list-rounded">
                    <li class="d-block">
                        <div class="form-check w-100">
                        <label class="form-check-label">
                            <input class="checkbox" type="checkbox"> Lorem Ipsum is simply dummy text of the printing <i class="input-helper rounded"></i>
                        </label>
                        <div class="d-flex mt-2">
                            <div class="ps-4 text-small me-3">24 June 2020</div>
                            <div class="badge badge-opacity-warning me-3">Due tomorrow</div>
                            <i class="mdi mdi-flag ms-2 flag-color"></i>
                        </div>
                        </div>
                    </li>
                    <li class="d-block">
                        <div class="form-check w-100">
                        <label class="form-check-label">
                            <input class="checkbox" type="checkbox"> Lorem Ipsum is simply dummy text of the printing <i class="input-helper rounded"></i>
                        </label>
                        <div class="d-flex mt-2">
                            <div class="ps-4 text-small me-3">23 June 2020</div>
                            <div class="badge badge-opacity-success me-3">Done</div>
                        </div>
                        </div>
                    </li>
                    <li>
                        <div class="form-check w-100">
                        <label class="form-check-label">
                            <input class="checkbox" type="checkbox"> Lorem Ipsum is simply dummy text of the printing <i class="input-helper rounded"></i>
                        </label>
                        <div class="d-flex mt-2">
                            <div class="ps-4 text-small me-3">24 June 2020</div>
                            <div class="badge badge-opacity-success me-3">Done</div>
                        </div>
                        </div>
                    </li>
                    <li class="border-bottom-0">
                        <div class="form-check w-100">
                        <label class="form-check-label">
                            <input class="checkbox" type="checkbox"> Lorem Ipsum is simply dummy text of the printing <i class="input-helper rounded"></i>
                        </label>
                        <div class="d-flex mt-2">
                            <div class="ps-4 text-small me-3">24 June 2020</div>
                            <div class="badge badge-opacity-danger me-3">Expired</div>
                        </div>
                        </div>
                    </li>
                    </ul>
                </div>
                </div>
            </div>
            </div>
        </div>
        </div>
    </div>
@endsection --}}

{{-- @section('amount')
    <div class="row flex-grow">
        <div class="col-12 grid-margin stretch-card">
        <div class="card card-rounded">
            <div class="card-body">
            <div class="row">
                <div class="col-lg-12">
                <div class="d-flex justify-content-between align-items-center mb-3">
                    <h4 class="card-title card-title-dash">Type By Amount</h4>
                </div>
                <canvas class="my-auto" id="doughnutChart" height="200"></canvas>
                <div id="doughnut-chart-legend" class="mt-5 text-center"></div>
                </div>
            </div>
            </div>
        </div>
        </div>
    </div>
@endsection --}}

{{-- @section('report')
    <div class="row flex-grow">
        <div class="col-12 grid-margin stretch-card">
        <div class="card card-rounded">
            <div class="card-body">
            <div class="row">
                <div class="col-lg-12">
                <div class="d-flex justify-content-between align-items-center mb-3">
                    <div>
                    <h4 class="card-title card-title-dash">Leave Report</h4>
                    </div>
                    <div>
                    <div class="dropdown">
                        <button class="btn btn-secondary dropdown-toggle toggle-dark btn-lg mb-0 me-0" type="button" id="dropdownMenuButton3" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Month Wise </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton3">
                        <h6 class="dropdown-header">week Wise</h6>
                        <a class="dropdown-item" href="#">Year Wise</a>
                        </div>
                    </div>
                    </div>
                </div>
                <div class="mt-3">
                    <canvas id="leaveReport"></canvas>
                </div>
                </div>
            </div>
            </div>
        </div>
        </div>
    </div>
@endsection --}}

@section('footer')
    <footer class="footer">
        <div class="d-sm-flex justify-content-center justify-content-sm-between">
        <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Produced by <a href="#">Delyre, LHAM & Octavel</a></span>
        <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Copyright © <?php echo date("Y"); ?> - ENEAM</span>
        </div>
    </footer>
@endsection

@section('script')
    <!-- plugins:js -->
    <script src="{{asset('assets_dashboard/vendors/js/vendor.bundle.base.js')}}"></script>
    <!-- endinject -->
    <!-- Plugin js for this page -->
    <script src="{{asset('assets_dashboard/vendors/chart.js/Chart.min.js')}}"></script>
    <script src="{{asset('assets_dashboard/vendors/progressbar.js/progressbar.min.js')}}"></script>

    <!-- End plugin js for this page -->
    <!-- inject:js -->
    <script src="{{asset('assets_dashboard/js/off-canvas.js')}}"></script>
    <script src="{{asset('assets_dashboard/js/hoverable-collapse.js')}}"></script>
    <script src="{{asset('assets_dashboard/js/template.js')}}"></script>
    <script src="{{asset('assets_dashboard/js/settings.js')}}"></script>
    <script src="{{asset('assets_dashboard/js/todolist.js')}}"></script>
    <!-- endinject -->
    <!-- Custom js for this page-->
    <script src="{{asset('assets_dashboard/js/jquery.cookie.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets_dashboard/js/dashboard.js')}}"></script>
    <script src="{{asset('assets_dashboard/js/Chart.roundedBarCharts.js')}}"></script>
    <!-- End custom js for this page-->
@endsection


