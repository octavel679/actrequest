<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class CodeController extends Controller
{
    public function index($choix, $mat){

        $code = DB::table('demandes')
                    ->select('code')
                    ->where('demandes.id_acte','=',$choix)
                    ->where('matricule','=',$mat)
                    ->value(0);

        return view('pages.code')
                ->with('code', $code);
    }
}
