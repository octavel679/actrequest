@extends('layout')
@extends('layout_navbar')

@section('head')
    <head>
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1.0" name="viewport">

        <title>ENEAM - Demande d'actes en ligne</title>
        <meta content="" name="description">
        <meta content="" name="keywords">

        <!-- Favicons -->
        <link href="{{asset('assets/img/favicon.png')}}" rel="icon">
        <link href="{{asset('assets/img/apple-touch-icon.png')}}" rel="apple-touch-icon">

        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

        <!-- Vendor CSS Files -->
        <link href="{{asset('assets/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{asset('assets/vendor/bootstrap-icons/bootstrap-icons.css')}}" rel="stylesheet">
        <link href="{{asset('assets/vendor/boxicons/css/boxicons.min.css')}}" rel="stylesheet">
        <link href="{{asset('assets/vendor/glightbox/css/glightbox.min.css')}}" rel="stylesheet">
        <link href="{{asset('assets/vendor/remixicon/remixicon.css')}}" rel="stylesheet">
        <link href="{{asset('assets/vendor/swiper/swiper-bundle.min.css')}}" rel="stylesheet">

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

        <!-- Template Main CSS File -->
        <link href="{{asset('stylepersonal.css')}}" rel="stylesheet">
    </head>
@endsection

@section('scolaire section')
    <div class="resume special2">
        <div class="container-special2">

            <div class="special2-title">
                <h2>Informations</h2>
                <p>A remplir !</p>
            </div>

            <form action="{{url('scolairesave/'.$choix.'/')}}" role="form" class="php-email-form mt-4" novalidate>
                @csrf
                <div class="row mt-2">

                    <h4><i class="bx bx-user"></i> Qui êtes vous ?</h4>
                    <div class="col-md-6 d-flex align-items-stretch">
                        <div class="col-md-12 form-group">
                            <input type="text" name="matricule" class="form-control" id="matricule" placeholder="Votre matricule" required>
                        </div>
                    </div>

                    <div class="col-md-6 mt-4 mt-md-0 d-flex align-items-stretch">
                        <div class="col-md-12 form-group">
                            <input type="email" name="email_etudiant" class="form-control" id="email" placeholder="Votre email" required>
                        </div>
                    </div>

                    <div class="col-md-6 mt-4 d-flex align-items-stretch">
                        <div class="col-md-12 form-group">
                            <input type="text" name="nom" class="form-control" id="name" placeholder="Votre nom" required>
                        </div>
                    </div>

                    <div class="col-md-6 mt-4 d-flex align-items-stretch">
                        <div class="col-md-12 form-group">
                            <input type="text" name="prenom" class="form-control" id="name" placeholder="Votre prénom" required>
                        </div>
                    </div>

                    <div class="col-md-6 mt-5 d-flex align-items-stretch">
                        <div class="info-box">
                            <h4><i class="bi bi-question-circle"></i> Autres informations</h4>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4 form-group">
                        <div class="col-md-12 form-group">
                            <input type="number" name="telephone" class="form-control" id="telephone" placeholder="Votre téléphone" required>
                        </div>
                    </div>

                    <div class="col-md-4 form-group">
                        <select class="form-control text-dark" id="classe" name="classe" required>
                            <option value="" disabled selected hidden>Votre classe</option>
                            @foreach($classes as $classe)
                                <option value="{{ $classe->id_de_classe }}">
                                    {{ $classe->libelle_classes }}
                                </option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-md-4 form-group">
                        <select class="form-control text-dark" id="sexe" name="sexe" required>
                            <option value="" disabled selected hidden>Votre sexe</option>
                            <option value="Masculin">Masculin</option>
                            <option value="Feminin">Féminin</option>
                            <option value="Autre">Autre</option>
                        </select>
                    </div>
                </div>

                <div class="my-3 mt-5">
                    <div class="loading mt-1"></div>
                    <div class="error-message"></div>
                </div>

                <div class="text-center">
                    <button class="contact php-email2-form btn btn-warning text-white" type="submit">Suivant</button>
                </div>
            </form>

            <script>
                // Example starter JavaScript for disabling form submissions if there are invalid fields
                (function() {
                    'use strict';
                    window.addEventListener('load', function() {
                        // Fetch all the forms we want to apply custom Bootstrap validation styles to
                        var forms = document.getElementsByClassName('php-email-form mt-4');
                        // Loop over them and prevent submission
                        var validation = Array.prototype.filter.call(forms, function(form) {
                            form.addEventListener('submit', function(event) {
                                if (form.checkValidity() === false) {
                                    event.preventDefault();
                                    event.stopPropagation();
                                }
                                form.classList.add('was-validated');
                            }, false);
                        });
                    }, false);
                })();
            </script>
        </div>
    </div>
@endsection

@section('designed')
    <div class="credits">
        Designed by <a href="#">Delyre, LHAM & Octavel</a>
    </div>
@endsection
