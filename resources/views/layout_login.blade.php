<!DOCTYPE html>
<html lang="en">

@yield('head')


  <body class="az-body">

    <div class="az-signin-wrapper">
      <div class="az-card-signin text-center">
        <h1><img src="assets/img/logo.png" style="width: 3.5rem; height : 3.5rem;"></h1>
        <div class="az-signin-header">
          <h2>Bienvenue !</h2>
          <h4>Connectez-vous</h4>

          <form action="{{url('/admin')}}">
            <div class="form-group text-left">
              <label style="font-weight: bold;" class="text-dark">ID</label>
              <input type="text" class="form-control" placeholder="Entrer votre ID" value="">
            </div><!-- form-group -->
            <div class="form-group text-left">
              <label style="font-weight: bold" class="text-dark">Mot de passe</label>
              <input type="password" class="form-control" placeholder="Entrer votre mot de passe" value="">
            </div><!-- form-group -->
            <button class="btn btn-az-primary btn-block">Se connecter</button>
          </form>
        </div><!-- az-signin-header -->
        <div class="az-signin-footer">
          <p><a href="" style="text-decoration: underline">Mot de passe oublié ?</a></p>
        </div><!-- az-signin-footer -->
      </div><!-- az-card-signin -->
    </div><!-- az-signin-wrapper -->

    <script src="../lib/jquery/jquery.min.js"></script>
    <script src="../lib/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="../lib/ionicons/ionicons.js"></script>
    <script src="{{asset('assets_dashboard/js/jquery.cookie.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets_dashboard/js/jquery.cookie.js')}}" type="text/javascript"></script>

    <script src="{{asset('assets_dashboard/js/azia.js')}}"></script>
    <script>
      $(function(){
        'use strict'

      });
    </script>
  </body>
</html>
