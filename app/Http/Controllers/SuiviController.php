<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\demande;
use Illuminate\Support\Facades\DB;

class SuiviController extends Controller
{
    public function index (){
        return view('pages.suivi');
    }

    public function checking (Request $requ){
        $mycode = $requ->input('code');

        $mycheck = DB::table('demandes')
                ->select('statut')
                ->where('code','=',$mycode)
                ->value(0);

        if($mycheck==0){
            $statutimage = "submit";
            $statuttexte = "Envoyé";
        } else if ($mycheck == 1){
            $statutimage = "processing";
            $statuttexte = "En cours";
        } else if ($mycheck == 2){
            $statutimage = "done";
            $statuttexte = "Terminé";
        }

        return view('pages.statut')
             ->with('mycheck',$mycheck)
             ->with('statuttexte',$statuttexte)
             ->with('statutimage',$statutimage);
    }
}
