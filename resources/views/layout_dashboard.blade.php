<!DOCTYPE html>
<html lang="en">

@yield('head')

<body>
  <div class="container-scroller">

    <!-- partial:partials/_navbar.html -->
    @yield('navbar')

    <!-- partial -->
    <div class="container-fluid page-body-wrapper">

      <!-- partial:partials/_settings-panel.html -->
      @yield('siderbar')

      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">
            <div class="col-sm-12">
              <div class="home-tab">

               @yield('idle')

                <div class="tab-content tab-content-basic">
                    <div class="tab-pane fade show active" id="idle" role="tabpanel" aria-labelledby="idle">

                        <div class="row">
                            <div class="col-sm-12">
                            <div class="statistics-details d-flex align-items-center justify-content-between">
                                <div>
                                <p class="statistics-title">Total</p>
                                <h3 class="rate-percentage">{{count($counttotals)}}</h3>
                                </div>
                                <div>
                                <p class="statistics-title">Diplômes</p>
                                <h3 class="rate-percentage">{{$count1diplome}}</h3>
                                </div>
                                <div>
                                <p class="statistics-title">Attestations</p>
                                <h3 class="rate-percentage">{{$count1attestation}}</h3>
                                </div>
                                <div class="d-none d-md-block">
                                <p class="statistics-title">Fiches</p>
                                <h3 class="rate-percentage">{{$count1fiche}}</h3>
                                </div>
                                <div class="d-none d-md-block">
                                <p class="statistics-title">Bulletins</p>
                                <h3 class="rate-percentage">{{$count1bulletin}}</h3>
                                </div>
                            </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12 d-flex flex-column">
                                <div class="row flex-grow">
                                <div class="col-12 grid-margin stretch-card">
                                    <div class="card card-rounded">
                                    <div class="mx-4">
                                        <div class="table-responsive  mt-1">
                                        <table class="table select-table">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Acte</th>
                                                    <th>Matricule</th>
                                                    <th>Nom & Prénoms</th>
                                                    <th>Sexe</th>
                                                    <th>Email</th>
                                                    <th>Date</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>

                                            <tbody>

                                                @foreach ($counttotals as $counttotal)
                                                    <tr>
                                                        <td>
                                                            <strong class="text-dark">#{{$counttotal->code}}</strong>
                                                        </td>
                                                        <td>
                                                            <h6 class="{{($counttotal->id_acte==1) ? 'text-danger' : 'text-warning'}}">{{$counttotal->libelle}}</h6>
                                                        </td>
                                                        <td>
                                                            <h6>{{$counttotal->matricule}}</h6>
                                                        </td>
                                                        <td>
                                                            <div class="d-flex ">
                                                                <div>
                                                                <h6>{{$counttotal->nom.' '.$counttotal->prenom}}</h6>

                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <h6 class="text-center">{{$counttotal->sexe}}</h6>
                                                        </td>
                                                        <td>
                                                            <h6>{{$counttotal->email_etudiant}}</h6>
                                                        </td>
                                                        <td>
                                                            <h6>{{$counttotal->created_at}}</h6>
                                                        </td>
                                                        <td>
                                                            <a onclick="TookAlert()" href="{{url('took/'.$counttotal->id.'/')}}" class="btn btn-info text-white">
                                                                Prendre
                                                            </a>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                </div>
                            </div>

                            <div class="col-lg-4 d-flex flex-column">

                                @yield('to_do')

                                @yield('amount')

                                @yield('report')

                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade show" id="current" role="tabpanel" aria-labelledby="current">

                        <div class="row">
                            <div class="col-sm-12">
                            <div class="statistics-details d-flex align-items-center justify-content-between">
                                <div>
                                <p class="statistics-title">Total</p>
                                <h3 class="rate-percentage">{{count($countcurrents)}}</h3>
                                </div>
                                <div>
                                <p class="statistics-title">Diplômes</p>
                                <h3 class="rate-percentage">{{$count2diplome}}</h3>
                                </div>
                                <div>
                                <p class="statistics-title">Attestations</p>
                                <h3 class="rate-percentage">{{$count2attestation}}</h3>
                                </div>
                                <div class="d-none d-md-block">
                                <p class="statistics-title">Fiches</p>
                                <h3 class="rate-percentage">{{$count2fiche}}</h3>
                                </div>
                                <div class="d-none d-md-block">
                                <p class="statistics-title">Bulletins</p>
                                <h3 class="rate-percentage">{{$count2bulletin}}</h3>
                                </div>
                            </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12 d-flex flex-column">
                                <div class="row flex-grow">
                                <div class="col-12 grid-margin stretch-card">
                                    <div class="card card-rounded">
                                    <div class="mx-4">
                                        <div class="table-responsive  mt-1">
                                        <table class="table select-table">
                                            <thead>
                                                <tr>
                                                    <th>Acte</th>
                                                    <th>Matricule</th>
                                                    <th>Nom & Prénoms</th>
                                                    <th>Email</th>
                                                    <th>Date</th>
                                                    <th>Action</th>
                                                    <th></th>
                                                </tr>
                                            </thead>

                                            <tbody>
                                                @foreach ($countcurrents as $countcurrent)
                                                    <tr>

                                                        <td>
                                                            <h6 class="{{($countcurrent->id_acte==1) ? 'text-info' : 'text-warning'}}">{{$countcurrent->libelle}}</h6>
                                                        </td>
                                                        <td>
                                                            <h6>{{$countcurrent->matricule}}</h6>
                                                        </td>
                                                        <td>
                                                            <div class="d-flex ">
                                                                <div>
                                                                <h6>{{$countcurrent->nom.' '.$countcurrent->prenom}}</h6>

                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <h6>{{$countcurrent->email_etudiant}}</h6>
                                                        </td>
                                                        <td>
                                                            <h6>{{$countcurrent->created_at}}</h6>
                                                        </td>
                                                        <td>
                                                            <a href="{{url('/admin/infos/'.$countcurrent->id.'/')}}" class="btn btn-otline-dark"><i class="icon-printer"></i> Voir plus</a>
                                                        </td>
                                                        <td>
                                                            <a onclick="FinishAlert()" href="{{url('done/'.$countcurrent->id.'/')}}" class="btn btn-success text-white">
                                                                Terminer
                                                            </a>
                                                        </td>
                                                        <td>
                                                            <a onclick="GiveUpAlert()" href="{{url('giveup/'.$countcurrent->id.'/')}}" class="btn btn-danger text-white">
                                                                Abandonner
                                                            </a>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                </div>
                            </div>

                            <div class="col-lg-4 d-flex flex-column">

                                @yield('to_do')

                                @yield('amount')

                                @yield('report')

                            </div>
                        </div>
                    </div>
                    {{-- <div class="tab-pane fade show" id="done" role="tabpanel" aria-labelledby="done">

                        <div class="row">
                            <div class="col-sm-12">
                            <div class="statistics-details d-flex align-items-center justify-content-between">
                                <div>
                                <p class="statistics-title">Total</p>
                                <h3 class="rate-percentage">{{$counttotal}}</h3>
                                </div>
                                <div>
                                <p class="statistics-title">Diplômes</p>
                                <h3 class="rate-percentage">18</h3>
                                </div>
                                <div>
                                <p class="statistics-title">Attestations</p>
                                <h3 class="rate-percentage">18</h3>
                                </div>
                                <div class="d-none d-md-block">
                                <p class="statistics-title">Fiches</p>
                                <h3 class="rate-percentage">18</h3>
                                </div>
                                <div class="d-none d-md-block">
                                <p class="statistics-title">Bulletins</p>
                                <h3 class="rate-percentage">18</h3>
                                </div>
                            </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12 d-flex flex-column">
                                <div class="row flex-grow">
                                <div class="col-12 grid-margin stretch-card">
                                    <div class="card card-rounded">
                                    <div class="mx-4">
                                        <div class="table-responsive  mt-1">
                                        <table class="table select-table">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Acte</th>
                                                    <th>Matricule</th>
                                                    <th>Nom & Prénoms</th>
                                                    <th>Sexe</th>
                                                    <th>Email</th>
                                                    <th>Date</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>

                                            <tbody>

                                                @foreach ($withnames as $withname)
                                                    <tr>
                                                        <td>
                                                            <strong class="text-dark">#{{$withname->code}}</strong>
                                                        </td>
                                                        <td>
                                                            <h6 class="{{($withname->id_acte==1) ? 'text-danger' : 'text-warning'}}">{{$withname->libelle}}</h6>
                                                        </td>
                                                        <td>
                                                            <h6>{{$withname->matricule}}</h6>
                                                        </td>
                                                        <td>
                                                            <div class="d-flex ">
                                                                <div>
                                                                <h6>{{$withname->nom.' '.$withname->prenom}}</h6>

                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <h6 class="text-center">{{$withname->sexe}}</h6>
                                                        </td>
                                                        <td>
                                                            <h6>{{$withname->email}}</h6>
                                                        </td>
                                                        <td>
                                                            <h6>{{$withname->created_at}}</h6>
                                                        </td>
                                                        <td>
                                                            <a onclick="TookAlert()" href="" class="btn btn-info text-white">
                                                                Prendre
                                                            </a>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                </div>
                            </div>

                            <div class="col-lg-4 d-flex flex-column">

                                @yield('to_do')

                                @yield('amount')

                                @yield('report')

                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade show" id="stats" role="tabpanel" aria-labelledby="stats">

                        <div class="row">
                            <div class="col-sm-12">
                            <div class="statistics-details d-flex align-items-center justify-content-between">
                                <div>
                                <p class="statistics-title">Total</p>
                                <h3 class="rate-percentage">{{$counttotal}}</h3>
                                </div>
                                <div>
                                <p class="statistics-title">Diplômes</p>
                                <h3 class="rate-percentage">18</h3>
                                </div>
                                <div>
                                <p class="statistics-title">Attestations</p>
                                <h3 class="rate-percentage">18</h3>
                                </div>
                                <div class="d-none d-md-block">
                                <p class="statistics-title">Fiches</p>
                                <h3 class="rate-percentage">18</h3>
                                </div>
                                <div class="d-none d-md-block">
                                <p class="statistics-title">Bulletins</p>
                                <h3 class="rate-percentage">18</h3>
                                </div>
                            </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12 d-flex flex-column">
                                <div class="row flex-grow">
                                <div class="col-12 grid-margin stretch-card">
                                    <div class="card card-rounded">
                                    <div class="mx-4">
                                        <div class="table-responsive  mt-1">
                                        <table class="table select-table">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Acte</th>
                                                    <th>Matricule</th>
                                                    <th>Nom & Prénoms</th>
                                                    <th>Sexe</th>
                                                    <th>Email</th>
                                                    <th>Date</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>

                                            <tbody>

                                                @foreach ($withnames as $withname)
                                                    <tr>
                                                        <td>
                                                            <strong class="text-dark">#{{$withname->code}}</strong>
                                                        </td>
                                                        <td>
                                                            <h6 class="{{($withname->id_acte==1) ? 'text-danger' : 'text-warning'}}">{{$withname->libelle}}</h6>
                                                        </td>
                                                        <td>
                                                            <h6>{{$withname->matricule}}</h6>
                                                        </td>
                                                        <td>
                                                            <div class="d-flex ">
                                                                <div>
                                                                <h6>{{$withname->nom.' '.$withname->prenom}}</h6>

                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <h6 class="text-center">{{$withname->sexe}}</h6>
                                                        </td>
                                                        <td>
                                                            <h6>{{$withname->email}}</h6>
                                                        </td>
                                                        <td>
                                                            <h6>{{$withname->created_at}}</h6>
                                                        </td>
                                                        <td>
                                                            <a onclick="TookAlert()" href="" class="btn btn-info text-white">
                                                                Prendre
                                                            </a>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                                </div>
                            </div>

                            <div class="col-lg-4 d-flex flex-column">

                                @yield('to_do')

                                @yield('amount')

                                @yield('report')

                            </div>
                        </div>
                    </div> --}}
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->

        @yield('footer')
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

@yield('script')
</body>

</html>
