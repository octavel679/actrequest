<!DOCTYPE html>
<html lang="en">
<head>
	<title>ENEAM - Demande d'acte</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
	<link rel="icon" type="image/ico" href="{{asset('assets/img/favicon.ico')}}"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('assets_suivi/vendor/bootstrap/css/bootstrap.min.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('assets_suivi/fonts/font-awesome-4.7.0/css/font-awesome.min.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('assets_suivi/vendor/animate/animate.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('assets_suivi/vendor/select2/select2.min.css')}}">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{asset('assets_suivi/css/util.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('assets_suivi/css/main.css')}}">
<!--===============================================================================================-->
</head>
<body>

	<!--  -->
	<div class="simpleslide100">
		<div class="simpleslide100-item bg-img1" style="background-image: url({{asset('assets/img/bg.jpg')}});"></div>
        <div class="simpleslide100-item bg-img1" style="background-image: url({{asset('assets/img/bg.jpg')}});"></div>
        <div class="simpleslide100-item bg-img1" style="background-image: url({{asset('assets/img/bg.jpg')}});"></div>
	</div>
    {{-- public\assets_suivi\images\bg01.jpg
    D:\Swipe\ENEAM\3ème année\UML Avancé\Projet\actrequest\public\assets_suivi\images\bg01.jpg --}}

	<div class="size1 overlay1">
		<!--  -->
		<div class="size1 flex-col-c-m p-l-15 p-r-15 p-t-50 p-b-50">
            <h3 class="l1-txt1 txt-center p-b-25">
                <a href="{{url('/')}}"><img src="{{asset('assets/img/logo.png')}}" style="width: 60px; height:60px;"></a>
				ENEAM
			</h3>

			<p class="m2-txt1 txt-center p-b-48">
				Suivez ici l'avancée de vos demandes d'acte en ligne !
			</p>


			<form class="w-full flex-w flex-c-m validate-form" action="suivi" method="post">
                @csrf
				<div class="wrap-input100 validate-input where1" data-validate = "Votre code de suivi n'est pas valide !">
					<input class="input100 placeholder0 s2-txt2" type="text" name="code" placeholder="Tapez votre code de suivi">
					<span class="focus-input100"></span>
				</div>

				<button type="submit" class="flex-c-m size3 s2-txt3 how-btn1 trans-04 where1">
					Vérifier
				</button>
			</form>
		</div>
	</div>





<!--===============================================================================================-->
	<script src="{{asset('assets_suivi/vendor/jquery/jquery-3.2.1.min.js')}}"></script>
<!--===============================================================================================-->
	<script src="{{asset('assets_suivi/vendor/bootstrap/js/popper.js')}}"></script>
	<script src="{{asset('assets_suivi/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
<!--===============================================================================================-->
	<script src="{{asset('assets_suivi/vendor/select2/select2.min.js')}}"></script>
<!--===============================================================================================-->
	<script src="{{asset('assets_suivi/vendor/tilt/tilt.jquery.min.js')}}"></script>
	<script >
		$('.js-tilt').tilt({
			scale: 1.1
		})
	</script>
<!--===============================================================================================-->
	<script src="{{asset('assets_suivi/js/main.js')}}"></script>

</body>
</html>
