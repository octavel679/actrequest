<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\Http\Requests;
use App\Models\demande;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RecapitulatifController extends Controller
{
    public function index($choix, $mat){
        $demandes = DB::table('demandes')
                    ->crossJoin('actes')
                    ->crossJoin('classes')
                    ->select('*')
                    ->where('demandes.id_acte','=',$choix)
                    ->where('demandes.id_acte','=',DB::raw('actes.id_acte'))
                    ->where('demandes.id_classe','=',DB::raw('classes.id_de_classe'))
                    ->where('matricule','=',$mat)
                    ->get();

        return view('pages.recapitulatif')
                ->with('id', $choix)
                ->with('matricule', $mat)
                ->with('demandes', $demandes);
    }
    
    public function finish($id, $matri, Request $request){
        //Generation du code de base
        $longueur = 3;
        $nombres = '0123456789';
        $lettres = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $longueurMaxNb = strlen($nombres);
        $longueurMaxLt = strlen($lettres);
        $chaineAleatoire = '';
        for ($i = 0; $i < $longueur; $i++){
            $chaineAleatoire .= $nombres[rand(0, $longueurMaxNb - 1)];
        }
        for ($i = 0; $i < $longueur; $i++){
            $chaineAleatoire .= $lettres[rand(0, $longueurMaxLt - 1)];
        }

        //Generation du code final
        $codes = preg_split('//', $chaineAleatoire, -1);
        shuffle($codes);

        $codeFinal = implode("",$codes);

        $data = array(
            "code" => $codeFinal,
        );

        DB::table('demandes')
            ->where('demandes.id_acte','=',$id)
            ->where('matricule','=',$matri)
            ->update($data);

        return redirect('/code/'.$choix.'/'.$mat);
    }
}
