<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppartientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appartients', function (Blueprint $table) {
            $table->id();
            $table->string('commentaire');
            $table->integer('id_acte')->unsigned();
            $table->foreign('id_acte')
                  ->references('id')
                  ->on('actes')
                  ->onDelete('restrict')
                  ->onUpdate('restrict');
            $table->integer('id_piece')->unsigned();
            $table->foreign('id_piece')
                    ->references('id')
                    ->on('pieces')
                    ->onDelete('restrict')
                    ->onUpdate('restrict');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appartients');
    }
}
