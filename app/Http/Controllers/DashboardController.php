<?php

namespace App\Http\Controllers;

use App\Models\demande;
use App\Models\acte;
use App\Models\utilisateur;
use App\Models\session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use LDAP\Result;

class DashboardController extends Controller
{
    public function login(){
        if (session('key')==1){
            return redirect('/admin');
        } else {
            return view('pages.login');
        }
    }

    public function treatment (Request $request) {
        $data = $request->input();

        $email = $request->input('email');
        $password = $request->input('password');

        $request->session()->put('user', $data['email']);

        $checkemail = DB::table('utilisateurs')
                    ->select('id')
                    ->where('email','=',$email)
                    ->where('password','=',$password)
                    ->count();

        $id = DB::table('utilisateurs')
                    ->select('id')
                    ->where('email','=',$email)
                    ->where('password','=',$password)
                    ->get();

        if ($checkemail==1){
            $request->session()->put('key', 1);

            $mysession = new session();
            $mysession-> user_id = $email;
            $mysession->save();

            return $this->index()
                ->with('id', $id);

        } else {
            return view ('pages.login');
        }
    }

    public function index(){
        if (session('key')==1){

            $idd=session('user');
            // $data['email'];
            // $request->session()->put('user', $data['email']);

            $iduser = DB::table('utilisateurs') // It's Okay !
                        ->select('*')
                        ->where('email','=', $idd)
                        ->value(0);

            $countcurrents = DB::table('demandes') //It's fnally Okay !
                            ->crossJoin('actes')
                            ->select('*')
                            ->where('demandes.id_utilisateur','=',$iduser)
                            ->where('demandes.statut','=',1)
                            ->where('demandes.id_acte','=',DB::raw('actes.id_acte'))
                            ->get();

            $counttotals = DB::table('demandes') //It's Okay !
                        ->crossJoin('actes')
                        ->select('*')
                        ->where('demandes.id_acte','=',DB::raw('actes.id_acte'))
                        ->where('demandes.id_utilisateur','=',NULL)
                        ->get();


                        $count1diplome4 = DB::table('demandes') //It's Okay !
                        ->select('*')
                        ->where('demandes.id_utilisateur','=',NULL)
                        ->where('demandes.id_acte','=',4)
                        ->count();
                        $count1diplome5 = DB::table('demandes') //It's Okay !
                        ->select('*')
                        ->where('demandes.id_utilisateur','=',NULL)
                        ->where('demandes.id_acte','=',5)
                        ->count();
                        $count1diplome = $count1diplome4 +$count1diplome5;


                        $count1attestation2 = DB::table('demandes') //It's Okay !
                        ->select('*')
                        ->where('demandes.id_utilisateur','=',NULL)
                        ->where('demandes.id_acte','=',2)
                        ->count();
                        $count1attestation3 = DB::table('demandes') //It's Okay !
                        ->select('*')
                        ->where('demandes.id_utilisateur','=',NULL)
                        ->where('demandes.id_acte','=',3)
                        ->count();
                        $count1attestation = $count1attestation2 +$count1attestation3;


                        $count2attestation2 = DB::table('demandes') //It's Okay !
                        ->select('*')
                        ->where('demandes.id_utilisateur','=',$iduser)
                        ->where('demandes.statut','=',1)
                        ->where('demandes.id_acte','=',2)
                        ->count();
                        $count2attestation3 = DB::table('demandes') //It's Okay !
                        ->select('*')
                        ->where('demandes.id_utilisateur','=',$iduser)
                        ->where('demandes.statut','=',1)
                        ->where('demandes.id_acte','=',3)
                        ->count();
                        $count2attestation = $count2attestation2 +$count2attestation3;


                        $count2diplome4 = DB::table('demandes') //It's fnally Okay !
                            ->select('*')
                            ->where('demandes.id_utilisateur','=',$iduser)
                            ->where('demandes.statut','=',1)
                            ->where('demandes.id_acte','=',4)
                            ->count();
                            $count2diplome5 = DB::table('demandes') //It's fnally Okay !
                            ->select('*')
                            ->where('demandes.id_utilisateur','=',$iduser)
                            ->where('demandes.statut','=',1)
                            ->where('demandes.id_acte','=',5)
                            ->count();
                        $count2diplome = $count2diplome4 + $count2diplome5; //It's fnally Okay !


                        $count1fiche = DB::table('demandes') //It's Okay !
                        ->select('*')
                        ->where('demandes.id_utilisateur','=',NULL)
                        ->where('demandes.id_acte','=',6)
                        ->count();

                        $count2fiche = DB::table('demandes') //It's Okay !
                        ->select('*')
                        ->where('demandes.id_utilisateur','=',$iduser)
                        ->where('demandes.statut','=',1)
                        ->where('demandes.id_acte','=',6)
                        ->count();


                        $count1bulletin = DB::table('demandes') //It's Okay !
                        ->select('*')
                        ->where('demandes.id_utilisateur','=',NULL)
                        ->where('demandes.id_acte','=',1)
                        ->count();

                        $count2bulletin = DB::table('demandes') //It's Okay !
                        ->select('*')
                        ->where('demandes.id_utilisateur','=',$iduser)
                        ->where('demandes.statut','=',1)
                        ->where('demandes.id_acte','=',1)
                        ->count();


            return view('pages.dashboard')
            // ,303,['count1diplome'=>$count1diplome,'count1fiche'=>$count1fiche,'count2fiche'=>$count2fiche,'count2diplome'=>$count2diplome,'count1attestation'=>$count1attestation,'count2attestation'=>$count2attestation,'counttotals'=>count($counttotals),'countcurrents'=>count($countcurrents)]);
                    ->with('counttotals', $counttotals)
                    ->with('count1diplome', $count1diplome)
                    ->with('count2diplome', $count2diplome)
                    ->with('countcurrents', $countcurrents)
                    ->with('count1attestation', $count1attestation)
                    ->with('count2attestation', $count2attestation)
                    ->with('count1fiche', $count1fiche)
                    ->with('count2fiche', $count2fiche)
                    ->with('count1bulletin', $count1bulletin)
                    ->with('count2bulletin', $count2bulletin);

        } else {
            return redirect('/login');
        }
    }

    public function logout(){
        if (session()->has('key')){
            session()->flush();
            return redirect('/login');
        }
    }

    public function show($id){
        $infoss = DB::table('demandes')
                    ->select('*')
                    ->where('id','=',$id)
                    ->get();

        return view('pages.infos')
        ->with('infoss', $infoss);
    }

    public function update($id){
        // $demande = demande::find($id);
        $idactuel = utilisateur::where('email',session('user'))->value('id');//It's Okay !

        $demande = demande::find($id);

        $demande->id_utilisateur = $idactuel;
        $demande->statut = 1;

        $demande->save();

        return redirect('/admin#current');
    }

    public function update_done($id){

        $demande = demande::find($id);

        $demande->statut = 2;

        $demande->save();

        return redirect('/admin#done');
    }

    public function update_giveup($id){

        $demande = demande::find($id);

        $demande->statut = 0;
        $demande->id_utilisateur = NULL;

        $demande->save();

        return redirect('/admin#done');
    }
}
